package com.finki.dians.reviews.service.impl;

import com.finki.dians.reviews.exception.ReviewNotFoundException;
import com.finki.dians.reviews.model.Review;
import com.finki.dians.reviews.repository.ReviewRepository;
import com.finki.dians.reviews.service.ReviewService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ReviewServiceImpl implements ReviewService {

    private final ReviewRepository reviewRepository;

    public ReviewServiceImpl(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    @Override
    public Page<Review> getAllForNode(String nodeId, Integer pageNumber, Integer pageSize) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);
        return this.reviewRepository.findAllByNodeId(nodeId, pageRequest);
    }

    @Override
    public void deleteReview(String id) {
        Optional<Review> persisted = this.reviewRepository.findById(id);
        if (persisted.isPresent()) {
            this.reviewRepository.deleteById(id);
        } else {
            throw new ReviewNotFoundException(String.format("Review with id %s not found", id));
        }
    }

    @Override
    public Review addReview(Review review) {
        review.setTimestamp(System.currentTimeMillis());
        return this.reviewRepository.save(review);
    }
}
