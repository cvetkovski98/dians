package com.finki.dians.users.services.impl;

import com.finki.dians.users.model.User;
import com.finki.dians.users.model.exceptions.DuplicateException;
import com.finki.dians.users.model.exceptions.NotFoundException;
import com.finki.dians.users.repository.UserRepository;
import com.finki.dians.users.services.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {


    private final UserRepository userRepository;

    @Override
    @Transactional
    public List<User> getUsers() {
        long start = System.currentTimeMillis();

        LOGGER.info("Retrieving users from database");

        List<User> userList = userRepository.findAll();

        LOGGER.info("Users retrieved from database in {} ms.", System.currentTimeMillis() - start);

        return userList;
    }

    @Override
    @Transactional
    public User getUserById(String userId) {
        long start = System.currentTimeMillis();

        LOGGER.info("Retrieving user from database");

        User user = userRepository.findById(userId).orElseThrow(() -> new NotFoundException(String.format("User with id: %s not found", userId)));

        LOGGER.info("User retrieved from database in {} ms.", System.currentTimeMillis() - start);

        return user;
    }

    @Override
    @Transactional
    public User createUser(User newUser) {
        long start = System.currentTimeMillis();

        LOGGER.info("Creating user in database");

        if (userRepository.existsByUsername(newUser.getUsername())) {
            throw new DuplicateException(String.format("User with this username: %s already exists", newUser.getUsername()));
        }

        newUser = userRepository.save(newUser);


        LOGGER.info("Users created in {} ms.", System.currentTimeMillis() - start);

        return newUser;

    }

    @Override
    @Transactional
    public User updateUser(User user) {
        long start = System.currentTimeMillis();

        LOGGER.info("Updating user: {}", user);

        user = userRepository.save(user);

        LOGGER.info("User updated in {} ms.", System.currentTimeMillis() - start);

        return user;
    }

    @Override
    @Transactional
    public void deleteUserById(String userId) {
        long start = System.currentTimeMillis();

        User user = this.getUserById(userId);

        LOGGER.info("Deleting user [{}]", userId);

        userRepository.delete(user);

        LOGGER.info("User deleted in {} ms.", System.currentTimeMillis() - start);
    }

}
