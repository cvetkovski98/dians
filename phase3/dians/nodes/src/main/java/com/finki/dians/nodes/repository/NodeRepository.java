package com.finki.dians.nodes.repository;

import com.finki.dians.nodes.model.Node;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface NodeRepository extends MongoRepository<Node, String>, CustomNodeRepository {

    @Override
    List<Node> findAll();

    @Override
    Optional<Node> findById(String id);

    @Override
    List<Node> customQueryAll(Query q);

    @Override
    Node save(Node node);

    @Override
    void deleteById(String id);
}
