package com.finki.dians.nodes.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class RequiredArgsMissingException extends RuntimeException {
    public RequiredArgsMissingException(String message) {
        super(message);
    }
}
