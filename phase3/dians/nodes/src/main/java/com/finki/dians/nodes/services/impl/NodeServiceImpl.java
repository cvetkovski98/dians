package com.finki.dians.nodes.services.impl;

import com.finki.dians.nodes.exception.*;
import com.finki.dians.nodes.model.*;
import com.finki.dians.nodes.repository.NodeRepository;
import com.finki.dians.nodes.services.NodeService;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
public class NodeServiceImpl implements NodeService {

    private final NodeRepository nodeRepository;

    public NodeServiceImpl(NodeRepository nodeRepository) {
        this.nodeRepository = nodeRepository;
    }

    @Override
    public Slice<Node> getAll(RangeFilter rangeFilter, NodeFilter nodeFilter, Integer pageNumber, Integer pageSize) {
        validateRange(rangeFilter);
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);
        Query q = this.createFilterQuery(rangeFilter, nodeFilter);
        return this.nodeRepository.customQueryAll(q, pageRequest);
    }

    @Override
    public List<Node> getAll(RangeFilter rangeFilter, NodeFilter nodeFilter) {
        validateRange(rangeFilter);
        Query q = this.createFilterQuery(rangeFilter, nodeFilter);
        return this.nodeRepository.customQueryAll(q);
    }

    @Override
    public Node getNode(String id) {
        return this.nodeRepository.findById(id)
                .orElseThrow(() -> new NodeNotFoundException(id));
    }

    @Override
    public Node updateNode(String id, Node node) {
        if (!id.equals(node.getId())) {
            throw new UriResourceMismatchException(String
                    .format("Path variable id %s and resource id %s do not match",
                            id,
                            node.getId()));
        }
        return this.nodeRepository.findById(id)
                .map((persisted) -> this.nodeRepository.save(node))
                .orElseThrow(() -> new NodeNotFoundException(id));
    }

    @Override
    public Node createNode(Node node) {
        if (doesExist(node)) {
            throw new NodeAlreadyExistsException(String
                    .format("Node with coordinates lat: %s and lon: %s already exists",
                            node.getLatitude(),
                            node.getLongitude()));
        } else if (!Map.isPositionValid(node.getLatitude(), node.getLongitude())) {
            throw new NodePositionNotValidException(String
                    .format("Node with coordinates lat: %s and lon: %s is out of the Skopje region",
                            node.getLatitude(),
                            node.getLongitude()));
        } else {
            return this.nodeRepository.save(node);
        }
    }

    @Override
    public void deleteNode(String id) {
        Optional<Node> persisted = this.nodeRepository.findById(id);
        if (persisted.isPresent()) {
            this.nodeRepository.deleteById(id);
        } else {
            throw new NodeNotFoundException(id);
        }
    }

    private boolean doesExist(Node node) {
        ExampleMatcher nodeMatcher = ExampleMatcher.matching()
                .withIgnoreCase()
                .withMatcher(Node.LON, ExampleMatcher.GenericPropertyMatcher::exact)
                .withMatcher(Node.LAT, ExampleMatcher.GenericPropertyMatcher::exact)
                .withIgnorePaths("id");
        Example<Node> nodeExample = Example.of(node, nodeMatcher);
        return this.nodeRepository.exists(nodeExample);
    }

    private Query createFilterQuery(RangeFilter rangeFilter, NodeFilter nodeFilter) {
        BoundingRect boundingRect = Map.getBoundingRect(rangeFilter);
        Criteria rangeCriteria = new Criteria().andOperator(
                Criteria.where(Node.LON).gte(boundingRect.getMinLon()).lte(boundingRect.getMaxLon()),
                Criteria.where(Node.LAT).gte(boundingRect.getMinLat()).lte(boundingRect.getMaxLat())
        );
        Criteria nameCriteria = new Criteria();
        Criteria typeCriteria = new Criteria();
        Criteria timeCriteria = new Criteria();

        String nameQuery = nodeFilter.getNameQuery();
        if (isParamValid(nameQuery)) {
            nameQuery = nameQuery.toLowerCase(Locale.ROOT).strip();
            String nameRegex = String.format(".*%s.*", nameQuery);
            nameCriteria = new Criteria().orOperator(
                    Criteria.where(Node.TAG_KEY).is(Node.NAME)
                            .and(Node.TAG_VALUE).regex(nameRegex, "i"),
                    Criteria.where(Node.TAG_KEY).is(Node.NAME_EN)
                            .and(Node.TAG_VALUE).regex(nameRegex, "i"),
                    Criteria.where(Node.TAG_KEY).is(Node.NAME_SQ)
                            .and(Node.TAG_VALUE).regex(nameRegex, "i"),
                    Criteria.where(Node.TAG_KEY).is(Node.NAME_ALT)
                            .and(Node.TAG_VALUE).regex(nameRegex, "i")
            );
        }


        String[] typeQueries = nodeFilter.getTypeQueries();
        List<Criteria> criteriaList = new ArrayList<>();
        boolean valid = typeQueries != null && typeQueries.length > 0;

        if (!valid) {
            typeQueries = new String[]{
                    "coffee", "restaurant", "club", "bar", "pub"
            };
        }
        
        for (String type : typeQueries) {
            type = type.toLowerCase(Locale.ROOT).strip();
            String typeRegex = String.format(".*%s.*", type);
            criteriaList.add(Criteria.where(Node.TAG_KEY).is(Node.AMENITY)
                    .and(Node.TAG_VALUE).regex(typeRegex, "i"));
        }
        typeCriteria.orOperator(criteriaList.toArray(new Criteria[0]));

        String hoursFrom = nodeFilter.getWorkHoursFrom();
        String hoursTo = nodeFilter.getWorkHoursTo();
        String timeRegex = null;
        boolean anyValid = false;
        if (isParamValid(hoursFrom) && isParamValid(hoursTo)) {
            hoursFrom = hoursFrom.toLowerCase(Locale.ROOT).strip();
            hoursTo = hoursTo.toLowerCase(Locale.ROOT).strip();
            timeRegex = String.format(".*%s-%s.*", hoursFrom, hoursTo);
            anyValid = true;
        } else if (isParamValid(hoursFrom)) {
            hoursFrom = hoursFrom.toLowerCase(Locale.ROOT).strip();
            timeRegex = String.format(".*%s-.*", hoursFrom);
            anyValid = true;
        } else if (isParamValid(hoursTo)) {
            hoursTo = hoursTo.toLowerCase(Locale.ROOT).strip();
            timeRegex = String.format(".*-%s.*", hoursTo);
            anyValid = true;
        }

        if (anyValid) {
            timeCriteria = Criteria.where(Node.TAG_KEY).is(Node.OPENING_HOURS)
                    .and(Node.TAG_VALUE).regex(timeRegex, "i");
        }

        Criteria criteria = new Criteria().andOperator(
                rangeCriteria,
                nameCriteria,
                typeCriteria,
                timeCriteria
        );

        return new Query(criteria);
    }

    private boolean isParamValid(String query) {
        return query != null && !query.isEmpty() && !query.isBlank();
    }

    private void validateRange(RangeFilter rangeFilter) {
        if (rangeFilter.getLat() == null || rangeFilter.getLng() == null)
            throw new RequiredArgsMissingException("Latitude and longitude are required arguments");
    }
}
