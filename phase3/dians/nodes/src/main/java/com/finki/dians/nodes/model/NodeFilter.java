package com.finki.dians.nodes.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NodeFilter {
    public String nameQuery;

    public String [] typeQueries;

    public String workHoursFrom;

    public String workHoursTo;
}
