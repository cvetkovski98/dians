package com.finki.dians.nodes.model;

public class Map {
    private static final Double minLon = 21.2918;
    private static final Double maxLon = 21.5614;
    private static final Double minLat = 41.9357;
    private static final Double maxLat = 42.0535;

    public static BoundingRect getBoundingRect(RangeFilter rangeFilter) {
        BoundingRect boundingRect = new BoundingRect();

        Double lat = rangeFilter.getLat();
        Double lon = rangeFilter.getLng();
        Double radius = rangeFilter.getRadius();

        Double minLatOffset = lat - radius;
        Double maxLatOffset = lat + radius;

        Double minLonOffset = lon - radius;
        Double maxLonOffset = lon + radius;

        if (maxLatOffset > Map.maxLat) {
            maxLatOffset = Map.maxLat;
        }
        if (minLatOffset < Map.minLat) {
            minLatOffset = Map.minLat;
        }
        if (maxLonOffset > Map.maxLon) {
            maxLonOffset = Map.maxLon;
        }
        if (minLonOffset < Map.minLon) {
            minLonOffset = Map.minLon;
        }

        boundingRect.setMaxLat(maxLatOffset);
        boundingRect.setMinLat(minLatOffset);
        boundingRect.setMaxLon(maxLonOffset);
        boundingRect.setMinLon(minLonOffset);

        return boundingRect;
    }

    public static boolean isPositionValid(Double lat, Double lon) {
        return lat >= Map.minLat && lat <= Map.maxLat &&
                lon >= Map.minLon && lon <= Map.maxLon;
    }
}
