package com.finki.dians.nodes.model;


import com.mongodb.lang.Nullable;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Document(collection = "nodes")
public class Node {

    @Id
    public String id;

    @NonNull
    @Field("lat")
    public Double latitude;

    @NonNull
    @Field("lon")
    public Double longitude;

    @Nullable
    public Long osm_id;

    @NonNull
    @Field("tag")
    public List<Tag> tags;

    @Transient
    public static final String LAT = "lat";

    @Transient
    public static final String LON = "lon";

    @Transient
    public static final String NAME = "name";

    @Transient
    public static final String NAME_EN = "name:en";

    @Transient
    public static final String NAME_SQ = "name:sq";

    @Transient
    public static final String NAME_ALT = "alt_name";

    @Transient
    public static final String AMENITY = "amenity";

    @Transient
    public static final String OPENING_HOURS = "opening_hours";

    @Transient
    public static final String TAG_KEY = "tag.key";

    @Transient
    public static final String TAG_VALUE = "tag.value";

}
