# pull official base image
FROM node:15.3.0-alpine AS build

WORKDIR /app

COPY package.json .

RUN yarn cache clean
RUN yarn install

ENV PATH="/app/node_modules/.bin:${PATH}"

COPY . .

RUN yarn build

FROM nginx:stable-alpine AS final

# Copy the react build from Stage 1
COPY --from=build /app/build /var/www

# Copy our custom nginx config
COPY nginx.conf /etc/nginx/nginx.conf

# Expose port 80 to the Docker host, so we can access it
# from the outside.
EXPOSE 80

ENTRYPOINT ["nginx","-g","daemon off;"]