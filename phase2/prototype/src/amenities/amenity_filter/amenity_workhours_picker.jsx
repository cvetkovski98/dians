import React from "react";
import {KeyboardTimePicker} from "@material-ui/pickers";
import {makeStyles} from "@material-ui/core/styles";
import {FormControl, FormGroup, FormLabel} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    picker: {
        width: '12rem',
        marginTop: theme.spacing(0),
        marginRight: theme.spacing(1),
    },
    group: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    }
}))

export const AmenityWorkHoursPicker = ({
                                           handleChange,
                                           hours,
                                           className
                                       }) => {
    const classes = useStyles()

    return (
        <FormControl className={className}>
            <FormLabel component={'legend'}>
                Work time
            </FormLabel>
            <FormGroup row className={classes.group}>
                <KeyboardTimePicker
                    margin="normal"
                    id="from"
                    name={"from"}
                    fullWidth
                    value={hours.from}
                    className={classes.picker}
                    onChange={(date_str) => handleChange("from", date_str)}
                    KeyboardButtonProps={{
                        'aria-label': 'change time',
                    }}
                />
                <KeyboardTimePicker
                    margin="normal"
                    id="to"
                    name={"to"}
                    fullWidth
                    value={hours.to}
                    className={classes.picker}
                    onChange={(date_str) => handleChange("to", date_str)}
                    KeyboardButtonProps={{
                        'aria-label': 'change time',
                    }}
                />
            </FormGroup>
        </FormControl>
    )

}
