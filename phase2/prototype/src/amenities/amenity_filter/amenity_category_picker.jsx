import React from "react";
import {Checkbox, FormControl, FormControlLabel, FormGroup, FormLabel} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
    fieldset: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
}))

export const AmenityCategoryPicker = ({className, onChange, categories}) => {
    const classes = useStyles()
    return (
        <FormControl component="fieldset" className={className}>
            <FormLabel component="legend">
                Check amenity tags
            </FormLabel>
            <FormGroup className={classes.fieldset}>
                {
                    categories && categories.map(({key, value}, i) => {
                        return (
                            <FormControlLabel key={i}
                                              onChange={onChange}
                                              control={<Checkbox checked={value}
                                                                 name={key}/>}
                                              label={key[0].toUpperCase() + key.substr(1)}/>
                        )
                    })
                }
            </FormGroup>
        </FormControl>
    )
}