import React from 'react';
import {makeStyles} from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import ReactStars from 'react-stars'

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        marginBottom: theme.spacing(1),
        cursor: 'pointer'
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
        width: 140,
        height: 140,
        borderRadius: '50%',
        margin: 10
    },
    controls: {
        display: 'flex',
        alignItems: 'center',
        paddingLeft: theme.spacing(2),
        paddingBottom: theme.spacing(1),
    },
    hours: {
        display: 'flex',
        alignItems: 'center',
        paddingLeft: theme.spacing(2),
        // paddingBottom: theme.spacing(1),
    },
    playIcon: {
        height: 38,
        width: 38,
    },
}));

export const AmenityCard = ({id, tags, onClick}) => {
    const classes = useStyles();

    const getTag = tag => {
        let value;
        if (tags)
            value = tags.find(x => x.key.includes(tag))
        return value && value.value;
    }

    const street = getTag('street');
    const name = getTag('name');
    const opening_hours = getTag('opening_hours');
    const stars = 5;
    return (
        <Card className={classes.root} onClick={() => onClick(id)}>
            <CardMedia
                className={classes.cover}
                image={`https://picsum.photos/200/300`}
                title="Live from space album cover"
            />
            <div className={classes.details}>
                <CardContent className={classes.content}>
                    <Typography component="h5" variant="h5" style={{wordWrap: "break-word"}}>
                        {
                            name ? name : ''
                        }
                    </Typography>
                    <Typography variant="subtitle1" color="textSecondary">
                        {street ? street : ''}
                    </Typography>
                </CardContent>

                {opening_hours ? opening_hours.split(new RegExp('[,;]'), 2).map((x, i) => {
                    return (
                        <Typography key={i}
                                    variant="subtitle1"
                                    color="textSecondary"
                                    className={classes.hours}>
                            {x}
                        </Typography>
                    )
                }) : ''}

                <div className={classes.controls}>
                    <ReactStars
                        count={5}
                        value={stars}
                        size={24}
                        edit={false}
                        color2={'#ffd700'}/>
                </div>
            </div>
        </Card>
    );
}
