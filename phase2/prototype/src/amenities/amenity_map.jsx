import React, {useEffect, useRef} from 'react';
import {AmenityActions} from "../store/actions/amenityActions";
import {useDispatch, useSelector} from "react-redux";
import {MapContainer, Marker, TileLayer, useMapEvents} from "react-leaflet";

const Map = ({lat, lng, zoom, amenities, onItemClick, isSetPinClicked}) => {
    const mapRef = useRef(null);
    const center = useSelector(state => state.amenities.mapCenter)
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(AmenityActions.fetchAllAmenities(center))
    }, [])

    const onMarkerDragEnd = (e) => {
        const lat = e.target._latlng.lat;
        const lng = e.target._latlng.lng;
        dispatch({
            type: "ADD_PIN", pinCoordinates: {lat, lng}
        })
    };

    return (
        <MapContainer
            style={{width: '100%', height: '100%', zIndex: 0}}
            center={center}
            zoom={zoom}
            ref={mapRef}
        >
            <MapEventHandlers/>
            <TileLayer
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            {
                isSetPinClicked ? <Marker
                        position={center}
                        draggable={true}
                        onDragEnd={(e) => onMarkerDragEnd(e)}
                        eventHandlers={{
                            dragend: (e) => {
                                onMarkerDragEnd(e)
                            },
                        }}
                    /> :
                    amenities && amenities.map((item, i) => {
                        return (
                            <div>
                                <Marker key={i}
                                        position={{lat: item.latitude, lng: item.longitude}}
                                        eventHandlers={{
                                            click: () => {
                                                onItemClick && onItemClick(item.id)
                                            },
                                        }}
                                />
                            </div>
                        )
                    })
            }
        </MapContainer>
    )
}

export default Map

function MapEventHandlers() {
    const dispatch = useDispatch();
    const filter = useSelector(state => state.amenities.filter);
    const map = useMapEvents({
        dragend() {
            const lng = map.getCenter().lng
            const lat = map.getCenter().lat
            dispatch(AmenityActions.updateMapCenter(lat, lng))
            dispatch(AmenityActions.fetchAllAmenitiesFiltered(filter,{lat, lng}))
        },
        click() {
        }
    })
    return null
}
