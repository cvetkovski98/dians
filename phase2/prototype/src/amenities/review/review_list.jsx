import React from "react";
import {ReviewItem} from "./review_item";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    root: {
        margin: theme.spacing(10, 0)
    },
    review: {
        marginBottom: theme.spacing(3)
    }
}))

export const ReviewList = ({reviews}) => {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            {

                reviews && reviews.map((rev, i) => {
                    return <ReviewItem key={i}
                                       reviewId={rev.id}
                                       stars={rev.rating}
                                       userId={rev.userId}
                                       content={rev.content}
                                       timestamp={rev.timestamp}
                                       className={classes.review}
                    />
                })
            }
        </div>
    )
}
