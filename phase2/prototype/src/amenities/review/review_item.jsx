import React from "react";
import ReactStars from "react-stars";
import {makeStyles} from "@material-ui/core/styles";
import {Typography} from "@material-ui/core";
import {format} from "date-fns"
import {useDispatch, useSelector} from "react-redux";
import {ReviewActions} from "../../store/actions/reviewActions";
import remove_svg from "../../assets/remove.svg";
import IconButton from "@material-ui/core/IconButton";

const useStyles = makeStyles(theme => ({
    root: {
        background: theme.palette.background.default,
        padding: theme.spacing(3),
        borderRadius: theme.spacing(3),
    },
    bolded: {
        fontWeight: 'bold'
    },
    padded: {
        marginTop: theme.spacing(3)
    },
    right: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingTop: theme.spacing(3)
    },
    button: {
        float: 'right'
    }
}))

export const ReviewItem = ({timestamp, userId, content, stars, className, reviewId}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.auth.currentUser);
    const deleteHandler = (reviewId) => {
        dispatch(ReviewActions.deleteReview(reviewId));
    }

    return (
        <div className={[className, classes.root].join(' ')}>
            {
                currentUser && currentUser.roles.includes('ROLES_ADMIN') &&
                <IconButton type={"submit"} className={classes.button}
                            onClick={() => deleteHandler(reviewId)}>
                    <img src={remove_svg} alt="Remove Icon" className={classes.img}/>
                </IconButton>
            }
            <Typography variant={'button'} className={classes.bolded}>
                {userId}
            </Typography>
            <Typography variant={"caption"} display={"block"}>
                {format(timestamp, "dd.MM.yyyy  HH:mm")}
            </Typography>
            <Typography variant={"body1"} className={classes.padded}>
                {content}
            </Typography>

            <div className={classes.right}>
                <ReactStars
                    count={5}
                    value={stars}
                    size={24}
                    edit={false}
                    color2={'#ffd700'}
                />
            </div>
        </div>
    )
}
