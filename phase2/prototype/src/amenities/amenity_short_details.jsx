import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import {Button, Typography} from "@material-ui/core";
import {useHistory} from 'react-router-dom'
import {AmenityImageFragment} from "./amenity_image_fragment";
import removeSvg from "../assets/remove.svg";
import IconButton from "@material-ui/core/IconButton";

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'absolute',
        top: '50%',
        right: 0,
        width: 550,
        zIndex: 1,
        padding: theme.spacing(4, 3),
        transform: "translateY(-50%)",
        overflow: "hidden",
        background: theme.palette.background.paper
    },
    img: {
        borderRadius: "50%",
        width: '180px',
        height: '180px',
        objectFit: "cover",
    },
    cta: {
        marginTop: theme.spacing(3)
    },
    button: {
        position: 'absolute',
        left: 10,
        top: 0
    }
}))

export const AmenityShortDetails = (props) => {
    const classes = useStyles();
    const {id, tags} = props.item
    const history = useHistory()
    const handleClick = () => {
        history.push(`/amenity/${id}`)
    }

    const getTag = tag => {
        let value;
        if (tags)
            value = tags.find(x => x.key.includes(tag))
        return value && value.value;
    }

    const street = getTag('street');
    const title = getTag('name');
    const stars = 5;
    const img = 'https://picsum.photos/200/300';
    const description = getTag('description');
    const openingHours = getTag('opening_hours')

    return (
        <Grid component={'div'} container className={classes.root} spacing={3}>
            <IconButton type={"submit"} className={classes.button}
                        onClick={() => {
                            props.onCancelClick()
                        }}>
                <img src={removeSvg} alt="Remove Icon"/>
            </IconButton>

            <AmenityImageFragment street={street}
                                  stars={stars}
                                  title={title}
                                  img={img}
                                  opening_hours={openingHours}
            />
            <Grid item xs={12}>
                <Typography variant={"body1"}>
                    {description}
                </Typography>
            </Grid>
            <Grid item xs={12} className={classes.cta}>
                <Button color={"primary"} variant={"contained"} fullWidth size={"large"} onClick={handleClick}>
                    Read more ...
                </Button>
            </Grid>
        </Grid>
    )
}
