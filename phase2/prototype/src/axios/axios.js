import axios from 'axios';

export const AUTH_TOKEN_KEY = 'auth_token';
const server_api = process.env.REACT_APP_SERVER_URL;
const instance = axios.create({
    baseURL: `${server_api}/api`,
    headers: {
        'Access-Control-Allow-Origin': '*'
    },
});

instance.interceptors.request.use(
    config => {
        const token = localStorage.getItem(AUTH_TOKEN_KEY);
        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        }
        return config;
    },
    error => Promise.reject(error)
);

export default instance;
