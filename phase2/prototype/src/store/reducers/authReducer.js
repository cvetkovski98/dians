import {CURRENT_USER, SIGN_IN, SIGN_OUT, UPDATE_TOKEN} from "../actions/actionTypes";
import {AUTH_TOKEN_KEY} from "../../axios/axios";
import {isExpired} from "react-jwt";

const initialState = {
    currentUser: null,
    token: null,
}

export const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case SIGN_IN:
            localStorage.setItem(AUTH_TOKEN_KEY, action.payload.token);
            localStorage.setItem(CURRENT_USER, JSON.stringify(action.payload.user))
            return {
                ...state,
                currentUser: action.payload.user,
                token: action.payload.token
            }
        case SIGN_OUT:
            localStorage.removeItem(AUTH_TOKEN_KEY)
            localStorage.removeItem(CURRENT_USER);
            return {
                ...state,
                currentUser: null,
                token: null
            }
        case UPDATE_TOKEN:
            let token = action.payload;
            let currentUser = null;
            const isExp = isExpired(token);

            if (!isExp) {
                localStorage.setItem(AUTH_TOKEN_KEY, token);
                currentUser = JSON.parse(localStorage.getItem(CURRENT_USER))
            } else {
                localStorage.removeItem(CURRENT_USER)
                localStorage.removeItem(AUTH_TOKEN_KEY)
                token = null
            }
            return {
                ...state,
                token: token,
                currentUser: currentUser
            }
        default:
            return {
                ...state
            }
    }
}
