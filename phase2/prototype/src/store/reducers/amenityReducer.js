import {FETCHED_AMENITIES, FETCHED_AMENITY, SET_FILTER, SET_MAP_CENTER} from "../actions/actionTypes";

const categories = ["coffee", "restaurant", "club", "bar", "pub"]
    .map(it => {
        return {
            key: it,
            value: false
        }
    })

const initialState = {
    amenities: [],
    reviews: [],
    user: {},
    mapCenter: {lat: 41.9981, lng: 21.4254},
    pinCoordinates: {lat: null, lng: null},
    filter: {
        from: null,
        to: null,
        nameQuery: "",
        categories
    }
}

export const amenityReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_MAP_CENTER:
            return {
                ...state,
                mapCenter: action.payload
            }
        case FETCHED_AMENITIES:
            return {
                ...state,
                amenities: action.amenities
            }
        case FETCHED_AMENITY:
            return {
                ...state,
                amenities: [...state.amenities, action.amenity]
            }
        case 'REMOVE_AMENITY':
            return {
                ...state,
                amenities: state.amenities.filter(x => x.id !== action.amenity.id)
            }
        case 'ADD_PIN':
            return {
                ...state,
                pinCoordinates: action.pinCoordinates
            }
        case SET_FILTER:
            return {
                ...state,
                filter: action.filter
            }
        default:
            return state
    }
}
