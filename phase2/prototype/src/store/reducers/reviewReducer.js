import {ADD_REVIEW, DELETE_REVIEW, FETCHED_REVIEWS} from "../actions/actionTypes";

export const reviewReducer = (state = {}, action) => {
    switch (action.type) {
        case FETCHED_REVIEWS:
            return {
                ...state,
                reviews: action.reviews
            };
        case ADD_REVIEW:
            return {
                ...state,
                reviews: [action.review, ...state.reviews]
            };
        case DELETE_REVIEW:
            return {
                ...state,
                reviews: state.reviews.filter(review => review.id !== action.reviewId)
            }
        default:
            return {
                ...state
            }
    }
}

