import {amenityReducer} from "./reducers/amenityReducer";
import {reviewReducer} from "./reducers/reviewReducer";
import {authReducer} from "./reducers/authReducer";
import {applyMiddleware, combineReducers, createStore} from "redux";
import thunk from "redux-thunk";

const rootReducer = combineReducers({
    amenities: amenityReducer,
    reviews: reviewReducer,
    auth: authReducer
})

export const store = createStore(rootReducer, applyMiddleware(thunk))