import React, {useState} from 'react'
import {Button, Checkbox, FormControlLabel, makeStyles, TextField, Typography} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import {useHistory} from 'react-router-dom'
import {useDispatch} from "react-redux";
import {AuthActions} from "../store/actions/authActions";

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "80vh"
    },
    form: {
        padding: theme.spacing(4),
        borderRadius: "15px",
        width: "25%",
    },
    form_field: {
        marginBottom: theme.spacing(3),
        backgroundColor: "white"
    },
    buttons: {
        display: "flex",
        alignItems: "center",
        justifyContent: "space-evenly",
        marginTop: theme.spacing(4)
    },
    btn: {
        width: "35%"
    }
}));

export const RegisterPage = () => {
    const classes = useStyles();
    const history = useHistory();
    const dispatch = useDispatch();
    const [form_data, set_form_data] = useState({
        email: "",
        username: "",
        password: "",
        confirm_password: "",
        confirm_terms: false
    })

    const handleChange = (e) => {
        const {name, value} = e.target
        set_form_data({
            ...form_data,
            [name]: value
        })
    }

    const handleCheck = (e) => {
        const {name, checked} = e.target
        set_form_data({
            ...form_data,
            [name]: checked
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        if (form_data.password !== form_data.confirm_password) {
            alert("Passwords do not match")
        } else if (!form_data.confirm_terms) {
            alert("Please accept terms of use")
        }
        dispatch(AuthActions.signUp(form_data.username, form_data.email, form_data.password, "user"))
        history.push('/login')
    }

    return (
        <div className={classes.root}>
            <Grid container className={classes.form} component={"form"} noValidate autoComplete="off" spacing={3}>
                <Grid item sm={12}>
                    <Typography variant={'h6'}>
                        Register form
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <TextField id="email"
                               name={"email"}
                               type={"email"}
                               label="Email"
                               variant="outlined"
                               fullWidth
                               value={form_data.email}
                               onChange={handleChange}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField id="username"
                               name={"username"}
                               label="Username"
                               variant="outlined"
                               fullWidth
                               value={form_data.username}
                               onChange={handleChange}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField id="password"
                               name={"password"}
                               label="Password"
                               variant="outlined"
                               type={"password"}
                               fullWidth
                               value={form_data.password}
                               onChange={handleChange}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField id="confirm_password"
                               name={"confirm_password"}
                               label="Confirm password"
                               variant="outlined"
                               type={"password"}
                               fullWidth
                               value={form_data.confirm_password}
                               onChange={handleChange}
                    />
                </Grid>
                <Grid item xs={12}>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={form_data.confirm_terms}
                                onChange={handleCheck}
                                name="confirm_terms"
                                color="primary"
                            />
                        }
                        label="I accept the terms of use"
                    />
                </Grid>
                <Grid item xs={12} className={classes.buttons}>
                    <Button variant={"contained"}
                            color={"primary"}
                            className={classes.btn}
                            type={"submit"}
                            onClick={handleSubmit}
                    >
                        Register
                    </Button>
                </Grid>
            </Grid>
        </div>
    )
}
