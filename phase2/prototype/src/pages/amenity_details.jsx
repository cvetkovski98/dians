import React, {useEffect} from 'react'
import {useParams} from 'react-router-dom'
import {AmenityImageFragment} from "../amenities/amenity_image_fragment";
import {Grid, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {ReviewList} from "../amenities/review/review_list";
import {ReviewForm} from "../amenities/review/review_form";
import {AmenityActions} from "../store/actions/amenityActions";
import {useDispatch, useSelector} from "react-redux";
import {ReviewActions} from "../store/actions/reviewActions";

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(6, 32)
    },
    details: {
        marginBottom: theme.spacing(5)
    },
    reviews: {},
    description: {
        // marginTop: theme.spacing(6)
    }
}))


export const AmenityDetailsPage = () => {
    const {amenity_id} = useParams()
    const classes = useStyles()
    const amenity = useSelector(
        state => state.amenities.amenities
            .find(amenity => amenity.id === amenity_id)
    );
    const reviews = useSelector(state => state.reviews.reviews);
    const dispatch = useDispatch();

    useEffect(() => {
        if (!amenity) {
            dispatch(AmenityActions.fetchAmenity(amenity_id))
        }
        dispatch(ReviewActions.fetchAllReviews(amenity_id));

    }, [amenity])

    const getTag = tag => {
        let value;
        if (amenity && amenity.tags)
            value = amenity.tags.find(x => x.key.includes(tag))
        return value && value.value;
    }

    const street = amenity && getTag('street');
    const title = amenity && getTag('name');
    const stars = 5;
    const img = 'https://picsum.photos/id/874/200/300';
    const description = getTag('description')
    const openingHours = amenity && getTag('opening_hours');

    return (
        <div className={classes.root}>
            <Grid container className={classes.details}>
                <Grid item xs={5}>
                    <AmenityImageFragment img={img}
                                          title={title}
                                          stars={stars}
                                          street={street}
                                          opening_hours={openingHours}
                    />
                </Grid>
                <Grid item className={classes.description} xs={7}>
                    <Typography variant={'h5'} color="textSecondary"
                                style={{marginBottom: '1.5rem'}}>
                        Description
                    </Typography>
                    <Typography variant={"body1"}>
                        {description}
                    </Typography>
                </Grid>
            </Grid>

            <ReviewList reviews={reviews}/>
            <ReviewForm nodeId={amenity_id}/>
        </div>
    )
}
