import {format} from 'date-fns'

export const convertToIso = (long) => {
    if (long)
        return format(long, 'hh:mm')
}
