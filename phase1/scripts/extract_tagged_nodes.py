import sys, json

"""
    Filter goals:
        1. Receive the node data in json format from previous filter
        2. Filter out nodes that do not contain tags: these nodes are merely building blocks for map roads and have no significant value for the project
        3. Create a snapshot of the data
        4. Pass the data in json format to the next filter
"""

# 1. receive data from standard input
standard_input = sys.stdin.read()

# 2.1. convert data in python dictionary
nodes = json.loads(standard_input)

# 2.2. filter out nodes with no tags
nodes_of_value = []
for node in nodes:
    if 'tag' in node.keys():
        nodes_of_value.append(node)

# 3.1. return data to json format
data_json = json.dumps(nodes_of_value)

# 3.2 create snapshot of the data
with open('data/map_data.snapshot.4.json', 'w') as f: 
    f.write(data_json)

# 4. pass data in json format in next filter
print(data_json)


