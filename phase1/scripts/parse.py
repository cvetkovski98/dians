import sys, xmltodict, json

"""
    Filter goals:
        1. Receive input from standard input in form of xml
        2. Parse the input from xml format into json format
        3. Create a snapshot of the data
        4. Pass the data in json format to the next filter
"""

# 1. receive data from standard input
standard_input = sys.stdin.read()

# 2.1. convert the data into python dictionary
xml_to_dict = xmltodict.parse(standard_input)

# 2.2. parse the data into json format
data_json = json.dumps(xml_to_dict)

# 3. create snapshot of the data
with open('data/map_data.snapshot.2.json', 'w') as f: 
    f.write(data_json)

# 4. pass the data to the next filter
print(data_json)