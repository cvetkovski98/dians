import sys, json

"""
    Filter goals:
        1. Receive the tagged nodes json format from previous filter
        2. Filter out nodes that do not contain tags with the key `amenity`
        3. Create a snapshot of the data
        4. Pass the data in json format to the next filter
"""

# 1. receive data from standard input
standard_input = sys.stdin.read()

# 2.1. convert data in python dictionary
nodes = json.loads(standard_input)

# 2.2. filter out nodes that are not amenities
amenity_nodes = []
for node in nodes:
    for tag in node['tag']:
        if isinstance(tag, dict) and tag['@k'] == 'amenity':
            amenity_nodes.append(node)
            break

# 3.1. return data to json format
data_json = json.dumps(amenity_nodes)

# 3.2 create snapshot of the data
with open('data/map_data.snapshot.5.json', 'w') as f: 
    f.write(data_json)

# 4. pass data in json format in next filter
print(data_json)


